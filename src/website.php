<?php

if (!function_exists('get_email')) {
    /**
     * Queries the database to get email and content and increments sent count
     */
    function get_email($match, $tags = []) {
        if ($email = db_find_cached(86400, 'emails', is_string($match) ? ['name' => $match] : $match, 1)) {
            if ($content = db_find_cached(60, 'email_contents', ['email_id' => $email['id']])) {
                //db_query_cached_single(60, 'SELECT * FROM email_contents WHERE email_id = ? ORDER BY sent ASC limit 1', $email['id'])
                $user_id = $tags['user_id'] ?? $tags['id'] ?? 0;

                $content_id = ['email_content_id' => $content['id']];
                $tags = array_merge(split_name($tags['name'] ?? ''), $tags, ['unsubscribe' => auth_url('/api/unsubscribe', $user_id, 'email_unsubscribe', $content_id), 'tracking' => sprintf('<img src="%s">', auth_url(url('/api/blank'), $user_id, 'email_open', $content_id))]);

                $button = function ($url, $text = 'Click here', $activity = 'link_click') use ($tags, $user_id, $content_id) {
                    $url = auth_url($url, $user_id, $activity, $content_id);
                    return sprintf('<a style="background-color:#008CBA;border:0 solid;border-radius:4px;color:white;padding:7px 25px;text-align:center;text-decoration:none;display:inline-block;font-size:16px;font-weight:bold;" href="%s">%s</a>', $url, $text);
                };

                $html = $content['html'];
                $html .= $email['promo'] ? "\n\n" . template(get_promo(), $tags) : '';
                $html .= $email['unsubscribe'] ? "\n\n<a href=\"{{unsubscribe}}\">Change subscription settings</a>'" : '';
                $html .= "\n\n{{tracking}}";

                $result['subject'] = template($content['subject'], $tags);
                $result['body'] = preg_replace('/(\r?\n){2,}/', "\n<br>\n<br>\n", template($html, $tags));
                $result['body'] = eval_php($result['body'], get_defined_vars());
            }
        }

        return $result ?? NULL;
    }
}

if (!function_exists('get_promo')) {
    /**
     * Gets the next promo
     */
    function get_promo() {
        $promo = db_query_cached_single(10, 'select * from promos where enabled = 1 order by sent limit 1');
        return template($promo['html'], $promo, TRUE);
    }
}

if (!function_exists('website_bootstrap')) {
    /**
     * Loads handlers most common routes
     */
    function website_bootstrap() {

    }
}

if (getenv('APP_BOOTED')) {
    website_bootstrap();
}