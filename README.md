## Installation

Just run `composer install [package_name]`

After that all these functions should be available in every PHP file (via composer's autoloader)

- [get_email](#get_email)

- [get_promo](#get_promo)

- [website_bootstrap](#website_bootstrap)

### get_email

Queries the database to get email and content and increments sent count

```php
function get_email($match, $tags = [])
```


### get_promo

Gets the next promo

```php
function get_promo()
```


### website_bootstrap

Loads handlers most common routes

```php
function website_bootstrap()
```


